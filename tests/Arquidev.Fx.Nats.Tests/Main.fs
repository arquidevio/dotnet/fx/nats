﻿namespace Arquidev.Fx.Nats

open Expecto

module TestApp =

    [<EntryPoint>]
    let main argv =
        Tests.runTestsInAssembly defaultConfig argv
