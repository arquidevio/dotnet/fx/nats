using Arquidev.Fx.Nats.Core;
using NATS.Client;

namespace Sandbox;

public class UpdateJsonTest : BackgroundService
{
    private readonly IConnection _connection;

    public UpdateJsonTest(IConnection connection)
    {
        _connection = connection;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        // test UpdateJson

        const string specialKey = "test736";
        var bucket = _connection.CreateKeyValueContext("test_bucket");
        bucket.CreateJson(specialKey, "dev");
        bucket.UpdateJson<string>(specialKey, dev => dev + "eloper");
        return Task.CompletedTask;
    }
}
