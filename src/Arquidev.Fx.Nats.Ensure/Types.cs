﻿using static Arquidev.Fx.Nats.Internal.Extensions;
using Arquidev.Fx.Hosting.StartupHooks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NATS.Client;
using NATS.Client.Internals;
using NATS.Client.JetStream;
using NATS.Client.KeyValue;

namespace Arquidev.Fx.Nats.Ensure;

public static class Extensions
{
    private class NatsEnsure
    {
    }

    public static IServiceCollection EnsureStream(this IServiceCollection services,
        Action<StreamConfiguration.StreamConfigurationBuilder> config)
    {
        return services.OnBeforeHostStarted<IConnection, ILogger<NatsEnsure>>(
            (typeof(Extensions).Namespace! + "." + nameof(EnsureStream)).ToLowerInvariant(),
            (connection, logger, _) =>
            {
                var jsm = connection.CreateJetStreamManagementContext();

                var builder = StreamConfiguration.Builder();
                builder.WithMaxAge(Duration.OfDays(28));
                config(builder);
                var streamConfig = builder.Build();
                try
                {
                    jsm.GetStreamInfo(streamConfig.Name);
                    logger.LogDebug("Stream {StreamName} already exists", streamConfig.Name);
                }
                catch (NATSJetStreamException ex) when (ex.ErrorCode == 404)
                {
                    logger.LogInformation("Stream {StreamName} not found. Creating", streamConfig.Name);
                    jsm.AddStream(streamConfig);
                }

                return Task.CompletedTask;
            });
    }

    public static IServiceCollection EnsureKvBucket(this IServiceCollection services,
        Action<KeyValueConfiguration.KeyValueConfigurationBuilder> config)
    {
        return services.OnBeforeHostStarted<IConnection, ILogger<NatsEnsure>>(
            (typeof(Extensions).Namespace! + "." + nameof(EnsureKvBucket)).ToLowerInvariant(),
            (connection, logger, _) =>
            {
                var kvm = connection.CreateKeyValueManagementContext();

                var builder = KeyValueConfiguration.Builder();
                config(builder);
                var kvBucketConfig = builder.Build();
                try
                {
                    kvm.GetStatus(kvBucketConfig.BucketName);
                    logger.LogDebug("Key-value bucket {BucketName} already exists", kvBucketConfig.BucketName);
                }
                catch (NATSJetStreamException ex) when (ex.ErrorCode == 404)
                {
                    logger.LogInformation("Key-value bucket {BucketName} not found. Creating",
                        kvBucketConfig.BucketName);
                    kvm.Create(kvBucketConfig);
                }

                return Task.CompletedTask;
            });
    }

    public static IServiceCollection EnsureDurableConsumer(this IServiceCollection services, string stream,
        Func<string, string>? consumer = null,
        Action<ConsumerConfiguration.ConsumerConfigurationBuilder>? config=null)
    {
        return services.OnBeforeHostStarted<IConnection, ILogger<NatsEnsure>>(
            (typeof(Extensions).Namespace! + "." + nameof(EnsureDurableConsumer)).ToLowerInvariant(),
            (connection, logger, _) =>
            {
                var jsm = connection.CreateJetStreamManagementContext();
                var builder = ConsumerConfiguration.Builder();
                var durableName = consumer?.Invoke(GetDurableName(stream)) ?? GetDurableName(stream);
                
                builder
                    .WithName(durableName)
                    .WithDurable(durableName)
                    .WithDeliverSubject(durableName)
                    .WithAckPolicy(AckPolicy.All)
                    .WithDeliverGroup(durableName)
                    .WithDeliverPolicy(DeliverPolicy.New);
                config?.Invoke(builder);
     
                var consumerConfig = builder.Build();
                try
                {
                    jsm.GetConsumerInfo(stream, consumerConfig.Durable);
                    logger.LogDebug("Consumer {Consumer} already exists", consumerConfig.Durable);
                }
                catch (NATSJetStreamException ex) when (ex.ErrorCode == 404)
                {
                    logger.LogInformation("Consumer {Consumer} not found. Creating",
                        consumerConfig.Durable);
                    jsm.AddOrUpdateConsumer(stream, consumerConfig);
                }

                return Task.CompletedTask;
            });
    }
}
