﻿using System.Reflection;

namespace Arquidev.Fx.Nats.Internal;

public static class Extensions
{
    public static string GetDurableName(string stream)
    {
        var asm = Assembly.GetEntryAssembly()!.GetName().Name!;
        return $"{stream}__{asm}".Replace(".", "_").ToLowerInvariant();
    }
}
