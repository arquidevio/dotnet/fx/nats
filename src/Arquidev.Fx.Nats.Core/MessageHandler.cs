using Microsoft.Extensions.Logging;
using NATS.Client;

namespace Arquidev.Fx.Nats.Core;

public abstract class MessageHandler<T> : RawMessageHandler, IHandler<T>
{
    protected MessageHandler(Subscribe subscribe, ILogger<RawMessageHandler> logger) : base(subscribe, logger)
    {
    }

    public override async Task Handle(Msg message, CancellationToken token) => await Handle(await Deserialize(message, token), token);
    protected abstract Task<T> Deserialize(Msg message, CancellationToken token);
    public abstract Task Handle(T message, CancellationToken token);
}
