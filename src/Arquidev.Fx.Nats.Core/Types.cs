﻿using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NATS.Client;
using NATS.Client.JetStream;
using NATS.Client.KeyValue;

namespace Arquidev.Fx.Nats.Core;

public class NatsSettings
{
    public string Server { get; init; } = null!;
    public string Token { get; init; } = null!;
}

public interface IHandler<in T>
{
    Task Handle(T message, CancellationToken token);
}

public interface IRawHandler
{
    Task Handle(Msg message, CancellationToken token);
}

public static partial class Extensions
{
    public static IServiceCollection AddNats(this IServiceCollection services) =>
        services
            .AddSingleton<IJetStream>(f =>
                f.GetRequiredService<IConnection>().CreateJetStreamContext()
            )
            .AddSingleton<IConnection>(f =>
            {
                var cfg = f.GetRequiredService<IConfiguration>();
                var settings = new NatsSettings();
                const string natsSectionKey = "nats";
                cfg.GetSection(natsSectionKey).Bind(settings);
                if (settings.Server == null)
                    throw new InvalidOperationException(
                        $"{natsSectionKey}.{nameof(NatsSettings.Server)} is not configured");
                var opts = ConnectionFactory.GetDefaultOptions(settings.Server);

                if (settings.Token == null)
                    throw new InvalidOperationException(
                        $"{natsSectionKey}.{nameof(NatsSettings.Token)} is not configured");
                opts.Token = settings.Token;

                opts.ClosedEventHandler += (_, args) => LoggingHandler("Closed", args);
                opts.DisconnectedEventHandler += (_, args) => LoggingHandler("Disconnected", args);
                opts.ReconnectedEventHandler += (_, args) => LoggingHandler("Reconnected", args);

                var factory = new ConnectionFactory();
                var connection = factory.CreateConnection(opts);
                f.GetRequiredService<IHostApplicationLifetime>().ApplicationStopping.Register(() =>
                {
                    connection.Drain();
                    connection.Dispose();
                });
                return connection;

                void LoggingHandler(string natsEvent, ConnEventArgs args)
                {
                    f.GetRequiredService<ILogger<IConnection>>()
                        .LogInformation("[NATS] {Event}, {ClientID}", natsEvent, args.Conn.ClientID);
                }
            });

    public static Task PublishJsonAsync<T>(this IJetStream jetStream, string subject, T msg,
        JsonSerializerOptions? options = null)
    {
        var bytes = JsonSerializer.SerializeToUtf8Bytes(msg, options);
        return jetStream.PublishAsync(subject, bytes);
    }

    public static ulong CreateJson<T>(this IKeyValue connection, object key, T value,
        JsonSerializerOptions? options = null)
        where T : notnull
    {
        return connection.Create(key.ToString(), JsonSerializer.SerializeToUtf8Bytes(value, options));
    }

    public static ulong UpdateJson<T>(this IKeyValue connection, object key, Func<T?, T?> updateFunc,
        JsonSerializerOptions? options = null)
        where T : notnull
    {
        var originalEntry = connection.Get(key.ToString());
        var originalValue = JsonSerializer.Deserialize<T>(originalEntry.Value, options);
        return connection.Update(key.ToString(), JsonSerializer.SerializeToUtf8Bytes(updateFunc(originalValue), options), originalEntry.Revision);
    }

    public static T? GetJson<T>(this IKeyValue connection, object key,
        JsonSerializerOptions? options = null)
        where T : notnull
    {
        var kvEntry = connection.Get(key.ToString());
        return kvEntry == null ? default : JsonSerializer.Deserialize<T>(kvEntry.Value, options);
    }
}
