using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NATS.Client;

namespace Arquidev.Fx.Nats.Core;

public abstract class RawMessageHandler: IHostedService, IRawHandler
{
    private readonly Subscribe _subscribe;
    private readonly ILogger<RawMessageHandler> _logger;
    private Task _process = Task.CompletedTask;
    private readonly CancellationTokenSource _localCts = new();
    private CancellationTokenSource? _linkedCts;

    protected RawMessageHandler(
        Subscribe subscribe,
        ILogger<RawMessageHandler> logger)
    {
        _subscribe = subscribe;
        _logger = logger;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _linkedCts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, _localCts.Token);

        var subscription = _subscribe();

        _process = Task.Run(async () =>
        {
            while (true)
            {
                try
                {
                    _linkedCts.Token.ThrowIfCancellationRequested();
                    var msg = subscription.NextMessage();
                    msg.Ack();

                    await Handle(msg, _linkedCts.Token)
                        .ConfigureAwait(false);
                }
                catch (OperationCanceledException)
                {
                    break;
                }
                catch (NATSTimeoutException) when (!subscription.IsValid)
                {
                    _logger.LogDebug("[NATS] Unsubscribed: {Stream}/{Subject}", subscription.Stream,
                        subscription.Subject);
                    break;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Message processing failed");
                }
            }
        }, _linkedCts.Token);
        return Task.CompletedTask;
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        try
        {
            _linkedCts?.Cancel();
            await _process.ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Fatal error");
        }
    }

    public abstract Task Handle(Msg message, CancellationToken token);
}
