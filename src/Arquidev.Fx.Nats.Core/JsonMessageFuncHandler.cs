using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace Arquidev.Fx.Nats.Core;

public class JsonMessageFuncHandler<T> : JsonMessageHandler<T>
{
    private readonly Handler<T> _handler;

    public JsonMessageFuncHandler(
        Subscribe subscribe,
        ILogger<JsonMessageHandler<T>> logger,
        Handler<T> handler,
        JsonSerializerOptions options
    ) : base(subscribe, options, logger)
    {
        _handler = handler;
    }

    public override Task Handle(T message, CancellationToken token) => _handler(message, token);
}

public class JsonMessageFuncHandler<T, TSvc> : JsonMessageFuncHandler<T>
{
    public JsonMessageFuncHandler(
        TSvc service,
        Handler<T, TSvc> handler,
        Subscribe subscribe,
        ILogger<JsonMessageHandler<T>> logger,
        JsonSerializerOptions options)
        : base(subscribe, logger, (m, t) => handler(m, service, t), options)
    {
    }
}

public class JsonMessageFuncHandler<T, TSvc, TSvc2> : JsonMessageFuncHandler<T, TSvc>
{
    public JsonMessageFuncHandler(
        TSvc2 service2,
        TSvc service,
        Handler<T, TSvc, TSvc2> handler,
        Subscribe subscribe,
        ILogger<JsonMessageHandler<T>> logger,
        JsonSerializerOptions options)
        : base(service, (m, svc, t) => handler(m, svc, service2, t), subscribe, logger, options)
    {
    }
}

public class JsonMessageFuncHandler<T, TSvc, TSvc2, TSvc3> : JsonMessageFuncHandler<T, TSvc, TSvc2>
{
    public JsonMessageFuncHandler(
        TSvc3 service3,
        TSvc2 service2,
        TSvc service,
        Handler<T, TSvc, TSvc2, TSvc3> handler,
        Subscribe subscribe,
        ILogger<JsonMessageHandler<T>> logger,
        JsonSerializerOptions options)
        : base(service2, service, (m, svc, svc2, t) => handler(m, svc, svc2, service3, t), subscribe, logger, options)
    {
    }
}
