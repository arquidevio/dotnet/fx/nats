using System.Collections.Concurrent;
using static Arquidev.Fx.Nats.Internal.Extensions;
using System.Text.Json;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NATS.Client;
using NATS.Client.JetStream;

namespace Arquidev.Fx.Nats.Core;

public static partial class Extensions
{
    public static IServiceCollection AddRawHandler<T>(this IServiceCollection services,
        string stream,
        string? subject = null,
        Func<string, string>? consumer = null) where T : class, IHostedService, IRawHandler =>
        services.AddSingleton<IHostedService, T>(
            f => ActivatorUtilities.CreateInstance<T>(f, AddSubscription(f, stream, subject, consumer)));

    public static IServiceCollection AddHandler<T, TMsg>(this IServiceCollection services,
        string stream,
        string? subject = null,
        Func<string, string>? consumer = null,
        JsonSerializerOptions? options = null)
        where T : class, IHandler<TMsg>, IHostedService =>
        services.AddSingleton<IHostedService, T>(
            f => ActivatorUtilities.CreateInstance<T>(f, AddSubscription(f, stream, subject, consumer),
                options ?? new JsonSerializerOptions()));

    public static IServiceCollection AddHandlerFunc<TMsg>(this IServiceCollection services,
        string stream,
        Handler<TMsg> handler,
        string? subject = null,
        Func<string, string>? consumer = null,
        JsonSerializerOptions? options = null)
    {
        return services
            .AddSingleton<IHostedService, JsonMessageFuncHandler<TMsg>>(f =>
                ActivatorUtilities.CreateInstance<JsonMessageFuncHandler<TMsg>>(f, handler,
                    AddSubscription(f, stream, subject, consumer),
                    options ?? new JsonSerializerOptions()));
    }

    public static IServiceCollection AddHandlerFunc<TMsg, TSvc>(this IServiceCollection services,
        string stream,
        Handler<TMsg, TSvc> handler,
        string? subject = null,
        Func<string, string>? consumer = null,
        JsonSerializerOptions? options = null)
    {
        return services
            .AddSingleton<IHostedService, JsonMessageFuncHandler<TMsg>>(f =>
                ActivatorUtilities.CreateInstance<JsonMessageFuncHandler<TMsg, TSvc>>(f,
                    AddSubscription(f, stream, subject, consumer), handler,
                    options ?? new JsonSerializerOptions()));
    }

    public static IServiceCollection AddHandlerFunc<TMsg, TSvc, TSvc2>(this IServiceCollection services,
        string stream,
        Handler<TMsg, TSvc, TSvc2> handler,
        string? subject = null,
        Func<string, string>? consumer = null,
        JsonSerializerOptions? options = null)
    {
        return services
            .AddSingleton<IHostedService, JsonMessageFuncHandler<TMsg>>(f =>
                ActivatorUtilities.CreateInstance<JsonMessageFuncHandler<TMsg, TSvc, TSvc2>>(f,
                    AddSubscription(f, stream, subject, consumer), handler,
                    options ?? new JsonSerializerOptions()));
    }

    public static IServiceCollection AddHandlerFunc<TMsg, TSvc, TSvc2, TSvc3>(this IServiceCollection services,
        string stream,
        Handler<TMsg, TSvc, TSvc2, TSvc3> handler,
        string? subject = null,
        Func<string, string>? consumer = null,
        JsonSerializerOptions? options = null)
    {
        return services
            .AddSingleton<IHostedService, JsonMessageFuncHandler<TMsg>>(f =>
                ActivatorUtilities.CreateInstance<JsonMessageFuncHandler<TMsg, TSvc, TSvc2, TSvc3>>(f,
                    AddSubscription(f, stream, subject, consumer), handler,
                    options ?? new JsonSerializerOptions()));
    }

    private static Subscribe AddSubscription(IServiceProvider f, string stream,
        string? subject, Func<string, string>? consumer = null)
    {
        var consumerKey = consumer?.Invoke(GetDurableName(stream)) ?? GetDurableName(stream);
        return Subscriptions.GetOrAdd(consumerKey, k =>
        {
            var jsm = f.GetRequiredService<IConnection>()
                .CreateJetStreamManagementContext();

            var config = jsm
                .GetConsumerInfo(stream, k)
                .ConsumerConfiguration;
            var jetStream = f.GetRequiredService<IJetStream>();
            var builder = PushSubscribeOptions.Builder();
            builder.WithConfiguration(config);
            var cfg = builder.Build();
            return () =>
            {
                var sub = jetStream.PushSubscribeSync(subject ?? config.FilterSubject, cfg.DeliverGroup, cfg);
                f.GetRequiredService<ILogger<Subscribe>>()
                    .LogDebug("[NATS] Subscribed to: {Subject} with: {Consumer}", subject ?? config.FilterSubject,
                        consumerKey);
                return sub;
            };
        });
    }

    private static readonly ConcurrentDictionary<string, Subscribe> Subscriptions = new();
}
