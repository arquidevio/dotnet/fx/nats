using System.Text.Json;
using Microsoft.Extensions.Logging;
using NATS.Client;

namespace Arquidev.Fx.Nats.Core;

public abstract class JsonMessageHandler<T> : MessageHandler<T>
{
    protected readonly JsonSerializerOptions Options;

    protected JsonMessageHandler(
        Subscribe subscribe,  JsonSerializerOptions options, ILogger<JsonMessageHandler<T>> logger) : base(subscribe, logger)
    {
        Options = options;
    }
    protected override Task<T> Deserialize(Msg message, CancellationToken token)
    {
        var payload =
            JsonSerializer.Deserialize<T>(message.Data, Options) ??
            throw new InvalidOperationException("Null message");
        return Task.FromResult(payload);
    }
}
