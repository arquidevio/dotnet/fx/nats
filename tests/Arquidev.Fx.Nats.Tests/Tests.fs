module Arquidev.Fx.Nats.Tests

open Arquidev.Fx.Nats.Core
open Expecto

[<Tests>]
let tests =
    testList "fix me" [ testTask "fix me" { "Fix me" |> Expect.equal true false } ]
