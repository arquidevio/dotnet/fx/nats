using Arquidev.Fx.Hosting.Core;
using Arquidev.Fx.Nats.Core;
using Arquidev.Fx.Nats.Ensure;
using NATS.Client;
using Sandbox;

var builder = WebApplication.CreateBuilder(args);
builder.Host.ConfigureDefaults();

builder.Services
    .AddNats()
    .EnsureStream(s => s.WithName("test").WithSubjects("test_subject"))
    .EnsureDurableConsumer("test")
    .AddHandler<TestHandler, Message>("test", "test_subject")
    .AddHostedService<UpdateJsonTest>();

Func<string, string> multiAll = d => $"{d}__all";
Func<string, string> justC = d => $"{d}__just_c";

builder.Services
    .EnsureStream(s => s.WithName(MultipleSubjects.Stream)
        .WithSubjects("multiple_subjects.a", "multiple_subjects.a.b", "multiple_subjects.c"))
    .EnsureDurableConsumer("multiple_subjects", consumer: multiAll,
        config: c => c.WithFilterSubject("multiple_subjects.>"))
    .EnsureDurableConsumer("multiple_subjects", consumer: justC,
        config: c => c.WithFilterSubject("multiple_subjects.c"))
    .EnsureKvBucket(b => b.WithName("test_bucket"))
    .AddHandlerFunc<MultipleSubjects.Message>(
        MultipleSubjects.Stream,
        (m, t) =>
        {
            Console.WriteLine("ALL: {0}", m);
            return Task.CompletedTask;
        }, consumer: multiAll
    ).AddHandlerFunc<MultipleSubjects.Message, IConnection>(
        MultipleSubjects.Stream,
        (m, c, t) =>
        {
            Console.WriteLine("JUST C: {0} - {1}", m, c.ConnectedId);

            return Task.CompletedTask;
        }, consumer: justC
    ).AddRawHandler<RawHandlerTest>("test");

var app = builder.Build();

app.Run();
