using NATS.Client.JetStream;

namespace Arquidev.Fx.Nats.Core;


public delegate IJetStreamPushSyncSubscription Subscribe();

public delegate Task Handler<in T>(T message, CancellationToken token);
public delegate Task Handler<in T, in TSvc1>(T message, TSvc1 svc, CancellationToken token);
public delegate Task Handler<in T, in TSvc1, in TSvc2>(T message,TSvc1 svc1, TSvc2 svc2, CancellationToken token);
public delegate Task Handler<in T, in TSvc1, in TSvc2, in TSvc3>(T message,TSvc1 svc1, TSvc2 svc2, TSvc3 svc3, CancellationToken token);
