namespace Sandbox;

public static class MultipleSubjects
{
    public const string Stream = "multiple_subjects";

    public record Message(int Value);
    public record Message2(int Value);
}
