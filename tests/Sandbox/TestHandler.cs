using System.Text.Json;
using Arquidev.Fx.Nats.Core;
using NATS.Client;

namespace Sandbox;

public class Message
{
    public string Value { get; set; } = "default";
}

public class TestHandler : JsonMessageHandler<Message>
{
    private readonly ILogger<JsonMessageHandler<Message>> _logger;

    public TestHandler(Subscribe subscribe, JsonSerializerOptions options, ILogger<JsonMessageHandler<Message>> logger) : base(subscribe, options, logger)
    {
        _logger = logger;
    }

    public override Task Handle(Message message, CancellationToken token)
    {
        _logger.LogInformation("Received: {Message}", message.Value);
        return Task.CompletedTask;
    }
}

public class RawHandlerTest : RawMessageHandler
{
    public RawHandlerTest(Subscribe subscribe, ILogger<RawMessageHandler> logger) : base(subscribe, logger)
    {
    }

    public override Task Handle(Msg message, CancellationToken token)
    {
        Console.WriteLine($"{message}");
        return Task.CompletedTask;
    }
}
